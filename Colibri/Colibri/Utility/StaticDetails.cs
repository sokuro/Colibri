﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colibri.Utility
{
    // static Paths
    public class StaticDetails
    {
        // Image Folder
        public const string DefaultProductImage = "default_product.jpg";
        public const string ImageFolder = @"img\ProductImage";

        // User Roles
        public const string AdminEndUser = "Admin";
        public const string SuperAdminEndUser = "Super Admin";
    }
}
